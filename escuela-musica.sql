-- CREAO LA BASE DE DATOS ESCUELA DE MUSICA
create database escuelaMusica;


-- SELECCIONO LA BBDD
use escuelaMusica;


-- CREO LA TABLA SOCIOS
create table socios (
    documentoID varchar(9) not null primary key,
    nombre varchar(30) not null,
    apellidos varchar(80) not null,
    residencia varchar(300) not null
);


-- CREO LA TABLA INSCRITOS
create table inscritos (
    asignatura varchar(50) not null,
    anio int(4) not null,
    cuotaPagada enum("si", "no"),
    documentoSocio varchar(9),
    primary key (documentoSocio, asignatura, anio)
);


-- INSERTO DIFERENTES REGISTROS A LA TABLA SOCIOS
insert into socios VALUES
    ("70099734K", "Guillermo", "Montero", "Collado Villalba, 23"),
    ("70067542H", "Ana", "de Armas", "Las Rozas, 24"),
    ("70088935M", "María", "López", "Las Matas, 64"),
    ("70077345S", "Roberto", "Esteban", "Torrelodones, 21"),
    ("70066534R", "Mario", "Sánchez", "Galapagar, 32"),
    ("70080635Q", "Irene", "Martín", "Morlzarzal, 31"),
    ("70053789L", "Aroa", "González", "Cerceda, 20"),
    ("70097435V", "Javier", "Menéndez", "Colmenarejo, 45");


-- INSERTO DIFERENTES REGISTROS A LA TABLA INSCRITOS
insert into inscritos VALUES
    ("Solfeo", 2019, "si", "70099734K"),
    ("Composición", 2019, "si", "70067542H"),
    ("Piano", 2019, "si", "70088935M"),
    ("Guitarra", 2019, "si", "70077345S"),
    ("Trompeta", 2019, "si", "70067542H"),
    ("Piano", 2019, "si", "70066534R"),
    ("Guitarra", 2019, "si", "70088935M"),
    ("Solfeo", 2019, "si", "70077345S"),
    ("Trompeta", 2019, "si", "70080635Q"),
    ("Composición", 2019, "si", "70053789L"),
    ("Guitarra", 2018, "si", "70088935M"),
    ("Composición", 2019, "si", "70080635Q"),
    ("Trompeta", 2019, "si", "70077345S"),
    ("Piano", 2019, "si", "70097435V");


-- MUESTRO EL NOMBRE DEL SOCIO Y TODAS LAS LECCIONES A LAS QUE ESTÉ INSCRITO (CON TODOS SUS ATRIBUTOS)
mysql> select s.nombre, i.asignatura, i.Año, i.cuotaPagada from socios s join inscritos i on s.documento = i.documentoSocio;

+-----------+--------------+------+-------------+
| nombre    | asignatura   | anio | cuotaPagada |
+-----------+--------------+------+-------------+
| Aroa      | Composición  | 2019 | si          |
| Mario     | Piano        | 2019 | si          |
| Ana       | Composición  | 2019 | si          |
| Ana       | Trompeta     | 2019 | si          |
| Roberto   | Guitarra     | 2019 | si          |
| Roberto   | Solfeo       | 2019 | si          |
| Roberto   | Trompeta     | 2019 | si          |
| Irene     | Composición  | 2019 | si          |
| Irene     | Trompeta     | 2019 | si          |
| María     | Guitarra     | 2018 | si          |
| María     | Guitarra     | 2019 | si          |
| María     | Piano        | 2019 | si          |
| Javier    | Piano        | 2019 | si          |
| Guillermo | Solfeo       | 2019 | si          |
+-----------+--------------+------+-------------+


-- MUESTRO EL NOMBRE DE LOS SOCIOS Y LAS ASIGNATURAS LOS CUALES ESTÁS INSCRITOS EN EL AÑO 2019.
mysql> select s.nombre, i.asignatura, i.anio from socios s join inscritos i on s.documentoID = i.documentoSocio where i.anio = 2019;

+-----------+--------------+------+
| nombre    | asignatura   | anio |
+-----------+--------------+------+
| Aroa      | Composición  | 2019 |
| Mario     | Piano        | 2019 |
| Ana       | Composición  | 2019 |
| Ana       | Trompeta     | 2019 |
| Roberto   | Guitarra     | 2019 |
| Roberto   | Solfeo       | 2019 |
| Roberto   | Trompeta     | 2019 |
| Irene     | Composición  | 2019 |
| Irene     | Trompeta     | 2019 |
| María     | Guitarra     | 2019 |
| María     | Piano        | 2019 |
| Javier    | Piano        | 2019 |
| Guillermo | Solfeo       | 2019 |
+-----------+--------------+------+


-- MUESTRO EL NOMBRE Y TODA LA INFORMACIÓN DE LAS INSCRIPCIONES DEL SOCIO CON NÚMERO DE DOCUMENTO '70088935M'.
mysql> select s.nombre, i.*  from socios s join inscritos i on s.documentoID = i.documentoSocio where i.documentoSocio = '70088935M';

+--------+------------+------+-------------+----------------+
| nombre | asignatura | anio | cuotaPagada | documentoSocio |
+--------+------------+------+-------------+----------------+
| María  | Guitarra   | 2018 | si          | 70088935M      |
| María  | Guitarra   | 2019 | si          | 70088935M      |
| María  | Piano      | 2019 | si          | 70088935M      |
+--------+------------+------+-------------+----------------+

