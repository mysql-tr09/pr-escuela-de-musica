BB.DD. - ESCUELA DE MÚSICA

Una academia  imparte clases de distintas asignaturas musicales.

En una tabla llamada "socios" guarda los datos de sus socios (documento identificativo, nombre y residencia).

En una tabla denominada "inscritos" almacena la información necesaria para las
inscripciones de los socios a los distintas lecciones (asignatura, año, cuotaPagada (valores SI o NO) y el documento del socio). Lecciones del tipo piano, guitarra, solfeo, ...
